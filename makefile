all: compile run

run: 
	java -jar rd881.jar -c 5000

compile:
	javac -cp rd881.jar *.java 

runspecial: compile
	java -jar rd881.jar -c 5000 -t 10

runspecial2: compile
	java -jar rd881.jar -c 5000 -t 1


.PHONY: run compile runspecial runspecial2
