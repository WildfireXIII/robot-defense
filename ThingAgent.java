import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;

import jig.misc.rd.AirCurrentGenerator;
import jig.misc.rd.Direction;
import jig.misc.rd.RobotDefense;


/**
 *  A simple agent that uses reinforcement learning to direct the vacuum
 *  The agent has the following critical limitations:
 *  
 *      - it only considers a small set of possible actions
 *      - it does not consider turning the vacuum off
 *      - it only reconsiders an action when the 'local' state changes  
 *        in some cases this may take a (really) long time
 *      - it uses a very simplisitic action selection mechanism
 *      - actions are based only on the cells immediately adjacent to a tower
 *      - action values are not dependent (at all) on the resulting state 
 */
public class ThingAgent extends BaseLearningAgent 
{

    /**
     * A Map of states to actions
     * 
     *  States are encoded in the StateVector objects
     *  Actions are associated with a utility value and stored in the QMap
     */
    HashMap<StateVector,QMap> actions = new HashMap<StateVector,QMap>();

    /**
     * The agent's sensor system tracks /how many/ insects a particular generator
     * captures, but here I want to know /when/ an air current generator just
     * captured an insect so I can reward the last action. We use this captureCount
     * to see when a new capture happens.
     */
    HashMap<AirCurrentGenerator, Integer> captureCount;

    /**
     * Keep track of the agent's last action so we can reward it
     */
    HashMap<AirCurrentGenerator, AgentAction> lastAction;
    HashMap<AirCurrentGenerator, StateVector> lastState;
  
    private List<Integer> stepsSinceLastAction = new ArrayList<Integer>();
	private List<Integer> lastActionThreshold = new ArrayList<Integer>();

	private List<Integer> stepsBugsCaught = new ArrayList<Integer>();
	private int stepNumber = 0;

	private int lastNumBugs = 0;

    private boolean isInitialized = false;
    
    /**
     * This stores the possible actions that an agent many take in any
     * particular state.
     */
    private static final AgentAction [] potentials;

    static 
    {
        Direction [] dirs = Direction.values();
        potentials = new AgentAction[dirs.length*2 + 1];

        int i = 0;
        for(Direction d: dirs) 
        {
            // creates a new directional action with the power set to full
            // power can range from 1 ... AirCurrentGenerator.POWER_SETTINGS
            //potentials[i] = new AgentAction(AirCurrentGenerator.POWER_SETTINGS, d);
            potentials[i] = new AgentAction(4, d);
            i++;
            //potentials[i] = new AgentAction(3, d);
            //i++;
            potentials[i] = new AgentAction(2, d);
            i++;
            //potentials[i] = new AgentAction(1, d);
            //i++;
        }
      
        potentials[i] = new AgentAction(0, dirs[0]);
    }
    
    public ThingAgent() 
    {
        captureCount = new HashMap<AirCurrentGenerator,Integer>();
        lastAction = new HashMap<AirCurrentGenerator,AgentAction>();         
        lastState = new HashMap<AirCurrentGenerator,StateVector>();         
    }
    
    /**
     * Step the agent by giving it a chance to manipulate the environment.
     * 
     * Here, the agent should look at information from its sensors and 
     * decide what to do.
     * 
     * @param deltaMS the number of milliseconds since the last call to step
     * 
     */
    public void step(long deltaMS) 
    {
		stepNumber++;
        if (!isInitialized) 
        { 
            for (AirCurrentGenerator acg : sensors.generators.keySet())  
			{ 
				stepsSinceLastAction.add(0); 
				lastActionThreshold.add(2);
			}
            isInitialized = true;
        }
        
        StateVector state;
        QMap qmap;

        // This must be called each step so that the performance log is 
        // updated.
        updatePerformanceLog();
        
        int acgIndex = 0;
        for (AirCurrentGenerator acg : sensors.generators.keySet()) 
        {
          
            if (!stateChanged(acg)) 
            {
                // increment number of steps since last action for this tower
                stepsSinceLastAction.set(acgIndex, stepsSinceLastAction.get(acgIndex) + 1);

                if (stepsSinceLastAction.get(acgIndex) < lastActionThreshold.get(acgIndex)) continue;
            }
          
            stepsSinceLastAction.set(acgIndex, 0);


            // Check the current state, and make sure member variables are
            // initialized for this particular state...
            state = thisState.get(acg);
            if (actions.get(state) == null) 
            {
                actions.put(state, new QMap(potentials));
            }
            if (captureCount.get(acg) == null) captureCount.put(acg, 0);


            // Check to see if an insect was just captured by comparing our
            // cached value of the insects captured by each ACG with the
            // most up-to-date value from the sensors
            boolean justCaptured;
            justCaptured = (captureCount.get(acg) < sensors.generators.get(acg));

            // if this ACG has been selected by the user, we'll do some verbose printing
            boolean verbose = (RobotDefense.getGame().getSelectedObject() == acg);
            
            //System.out.println(sensors.generators.get(acg));
			


            // If we did something on the last 'turn', we need to reward it
            if (lastAction.get(acg) != null ) 
            {
				Direction lastDirection = lastAction.get(acg).getDirection();
				System.out.println(lastDirection.toString());

                // get the action map associated with the previous state
                qmap = actions.get(lastState.get(acg));
                
                int numBugs = state.hashCode() - acg.getGridHeight() - acg.getGridWidth() - acg.getClass().hashCode();
                System.out.println("Number of bugs: " + numBugs);

				if (numBugs > 0) { lastActionThreshold.set(acgIndex, 5); }
				else { lastActionThreshold.set(acgIndex, 2); }

                if (justCaptured)
                {
                    // capturing insects is good
                    AgentAction last = lastAction.get(acg);
                    //int lastPower = last.getPower();
                    Direction lastDir = last.getDirection();

                    /*AgentAction[] actions = qmap.getActions();
                    
                    for (int i = 0; i < actions.length; i++)
                    {
                        if (actions[i].getDirection() == lastDir) 
                        {
                            System.out.println("Rewarding similar function..."); 
                            qmap.rewardAction(actions[i], 8.0, this.actions, state);
                        }
                    }*/
                    
                    qmap.rewardAction(lastAction.get(acg), 50.0, this.actions, state);
                    captureCount.put(acg,sensors.generators.get(acg));
                }
				else if (lastState.get(acg).areBugsInDirection(lastDirection))
				{
                    qmap.rewardAction(lastAction.get(acg), 5.0, this.actions, state);
				}
				else if (!lastState.get(acg).areBugsInDirection(lastDirection) && numBugs > 0 && lastAction.get(acg).getPower() != 0)
				{
                    qmap.rewardAction(lastAction.get(acg), -5.0, this.actions, state);
				}
				else if (lastNumBugs != 0 && numBugs == 0 && !justCaptured)
				{
					System.out.println("Failed to capture bug");
					qmap.rewardAction(lastAction.get(acg), -10.0, actions, state); 
				}

                // if the last action we took didn't lead to capturing, penalize
                else if (!justCaptured && numBugs > 0)
                {
                    System.out.println("Penalizing failure to capture bug");
                    qmap.rewardAction(lastAction.get(acg), -1.0, actions, state);
                }
              
                // reward turning off if no bugs around
                //else if (sensors.generators.get(acg) == 0 && lastAction.get(acg).getPower() == 0)

                else if (numBugs == 0 && lastAction.get(acg).getPower() == 0)
                {
                    System.out.println("Rewarding shutting off vacuum");
                    //qmap.rewardAction(lastAction.get(acg), 1.0);
                    qmap.rewardAction(lastAction.get(acg), 10.0, actions, state); // previously 10, actulaly 4
                }

                // if there are no bugs around, and it's not off, that's bad
                else if (numBugs == 0 && lastAction.get(acg).getPower() != 0)
                {
                    System.out.println("Penalizing not shutting off vacuum");
                    qmap.rewardAction(lastAction.get(acg), -20.0, actions, state); // previously -10 
                }
                else 
                {
                    qmap.rewardAction(lastAction.get(acg), 0.0, actions, state);
                }

				lastNumBugs = numBugs;
                
                // conversely, if there ARE bugs nearby and we turned off,
                // that's bad
                // else if (numbBugs 

                if (verbose) 
                {
                    System.out.println("Last State for " + acg.toString() );
                    System.out.println(lastState.get(acg).representation());
                    System.out.println("Updated Last Action: " + qmap.getQRepresentation());
                }
            } 

            // decide what to do now...
            // first, get the action map associated with the current state
            qmap = actions.get(state);

            if (verbose) 
            {
				System.out.println("This State for Tower " + acg.toString() );
				System.out.println(thisState.get(acg).representation());
			}

			//double thresh = .5 - .45*((double)stepNumber/(double)1000);
			//if (thresh < .05) { thresh = .05; }
			double thresh = .5 - .40*((double)stepNumber/(double)1000);
			if (thresh < .1) { thresh = .1; }
			System.out.println(stepNumber);
			System.out.println(thresh);
			
			// find the 'right' thing to do, and do it.
			AgentAction bestAction = qmap.findBestAction(verbose, lastAction.get(acg), false, thresh);
			bestAction.doAction(acg);

            // finally, store our action so we can reward it later.
            lastAction.put(acg, bestAction);
			lastState.put(acg, state);

            System.out.println(qmap.getQRepresentation());

        }
    }


    /**
     * This inner class simply helps to associate actions with utility values
     */
    static class QMap 
    {
    	private AgentAction lastValue;
		static Random RN = new Random();

        private double[] qvalues;       // current utility estimate
        private int[] attempts;         // number of times action has been tried
        private AgentAction[] actions;  // potential actions to consider
        private StateVector[] successors; 

        public QMap(AgentAction[] potential_actions) 
        {

            actions = potential_actions.clone();
            int len = actions.length;

            qvalues = new double[len];
            attempts = new int[len];
			successors = new StateVector[len];
            for(int i = 0; i < len; i++) 
            {
                qvalues[i] = 0.0;
                attempts[i] = 0;
                successors[i] = null;
            }
        }

        /**
         * Finds the 'best' action for the agent to take.
         * 
         * @param verbose
         * @return
         */
		public AgentAction findBestAction(boolean verbose, AgentAction lastAction, boolean forceBest, double randThresh) // forceBest if you need to know the actual best for calculating Q
        {
            int i,maxi,maxcount;
            maxi=0;
            maxcount = 1;

			AgentAction selectedAction = null;
            
            if (verbose)
                System.out.print("Picking Best Actions: " + getQRepresentation());

            for (i = 1; i < qvalues.length; i++) 
            {
                if (qvalues[i] > qvalues[maxi]) 
                {
                    maxi = i;
                    maxcount = 1;
                }
                else if (qvalues[i] == qvalues[maxi]) 
                {
                    maxcount++;
                }
            }
            //if (RN.nextDouble() > .2 || forceBest) 
			System.out.println("Hello " + randThresh);
            if (RN.nextDouble() > randThresh || forceBest) 
            {
				System.out.println("DOING  BEST");
                int whichMax = RN.nextInt(maxcount);

                if (verbose)
                    System.out.println( " -- Doing Best! #" + whichMax);

                for (i = 0; i < qvalues.length; i++)
                {
                    if (qvalues[i] == qvalues[maxi]) 
                    {
						//if (whichMax == 0) return actions[i];
						selectedAction = actions[i];
						whichMax--;
						break;
					}
				}

				if (lastAction != null) 
				{
					for(i = 0; i < qvalues.length; i++)
					{
						if(qvalues[i] == qvalues[maxi] && i != maxi && actions[i].getPower() == lastAction.getPower() && actions[i].getDirection().toString().equals(lastAction.getDirection().toString()))
						{
							System.out.println("\n\n\n\n========= RETAINING PREVIOUS ACTION =========\n\n\n\n");
							//return actions[i];
							selectedAction = actions[i];
						}
					}
				}

				//return actions[maxi];
				return selectedAction;
			}
			else 
            {
                int which = RN.nextInt(actions.length);
                if (verbose)
                    System.out.println( " -- Doing Random (" + which + ")!!");

                return actions[which];
            }
        }

        public AgentAction[] getActions() { return actions; }
        
        
        public double getQValue(AgentAction a)
        {
            int i;
            for (i = 0; i < actions.length; i++) 
            {
                if (a == actions[i]) break;
            }
            if (i >= actions.length) 
            {
                System.err.println("ERROR: Tried to reward an action that doesn't exist in the QMap. (Ignoring reward)");
                return 0.0;
            }
            
            return qvalues[i];
        }

        /**
         * Modifies an action value by associating a particular reward with it.
         * 
         * @param a the action performed 
         * @param value the reward received
         */
        public void rewardAction(AgentAction a, double value, HashMap<StateVector,QMap> successorQMaps, StateVector currentState) // use current state to add successor if not existant
        {
            //double alpha = .05;
            //double gamma = .05;
            double alpha = .1;
            double gamma = .2;
         
            int i;
            for (i = 0; i < actions.length; i++) 
            {
                if (a == actions[i]) break;
            }
            if (i >= actions.length) 
            {
                System.err.println("ERROR: Tried to reward an action that doesn't exist in the QMap. (Ignoring reward)");
                return;
            }
            
            // assign successor if we haven't already
            if (successors[i] == null) { successors[i] = currentState; }
            
            // find qmap for currentState (successor state)
            QMap successorMap = successorQMaps.get(currentState);
			double nextBestValue = 0.0;
			if (successorMap != null) { nextBestValue = successorMap.getQValue(successorMap.findBestAction(false, a, true, 0.0)); }
            
            attempts[i] = attempts[i] + 1;
            //qvalues[i] = qvalues[i] + alpha*(attempts[i])*(value + gamma * nextBestValue - qvalues[i]);
            qvalues[i] = qvalues[i] + alpha*(value + gamma * nextBestValue - qvalues[i]);

        }
        /**
         * Gets a string representation (for debugging).
         * 
         * @return a simple string representation of the action values
         */
        public String getQRepresentation() 
        {
            StringBuffer sb = new StringBuffer(80);

            for (int i = 0; i < qvalues.length; i++) 
            {
                sb.append(String.format("%.2f  ", qvalues[i]));
            }
            return sb.toString();

        }

    }
}




